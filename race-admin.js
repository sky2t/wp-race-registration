(function( $ ) {
    $(function(){
        var form = $('#form'),
            formHeight = form.height(),
            formBody = form.find('.form-body'),
            pro = false,
            data = [];

        $('.race-table tbody tr').click(function(el){

            if(!el.target.classList.contains('del')){
                var post = $(this).data('post'),
                    scroll = $(document).scrollTop();

                data = $(this).data('info');
                data.event = post.name;
                data.excert = post.excert;
                data.thumb = post.thumbnail;
                data.reg_date = $(this).data('reg');

                pro = $(this).hasClass('pro') ? true : false;

                print();

                formBody.css('top', scroll);
                form.css('height', formHeight + scroll).show();
            }else{
                var route = ajaxurl + '?action=del_request&id=' + el.currentTarget.id;
                $(el.currentTarget).find('span.del').css('color', 'gray');
                if (window.confirm("Are you sure?")) {
                    $.ajax({
                        url: route
                    }).done(function(e) {
                        if(e == 1){
                            $(el.currentTarget).remove();
                        }
                    });
                } // Are you sure
            }
        });

        function print(){
//console.log(data);
            for (var i = 0; i < 2; i++) {
                $.each(data, function(key, val){
                    if(i == 0){
                        $('.'+key).text(' ');
                    }else{
//                        console.log(key + ' - ' + val);
                        $('.'+key).append(' '+val);
                    }
                });
            }

            if(pro){
                form.find('.regular').hide();
                form.find('.pro').show();
            }else{
                form.find('.regular').show();
                form.find('.pro').hide();
            }


        }

        form.click(function(e){
            if(e.target.id == 'form'){
                form.hide();
            }
        });

    }); // ready
})(jQuery);


function reg_print(){
    var divToPrint= document.getElementById('form').getElementsByClassName('content')[0];

    var newWin=window.open('','Print-Window');

    newWin.document.open();

    newWin.document.write('<html><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');

    newWin.document.close();

    setTimeout(function(){newWin.close();},10);
}