<?php
require_once ('registration.php');

function race_reg($atts){
    $pro = false;
    if(isset($atts['form']) && $atts['form'] === 'pro'){
        $pro = true;
    }

    $id = get_post()->ID;
    $reg = new registration();

    if($_POST['reg_data']){

        if($_POST['iform'] == 'on'){
            $reg->subscribe($_POST['reg_data']);
        }

        $reg->addData($id, $_POST['reg_data']);

        $redirect = '<meta http-equiv="refresh" content="0; URL=".$_SERVER["REQUEST_URI"]." />';

//        wp_safe_redirect($_SERVER['REQUEST_URI']);
        exit($redirect);
    }

    
    ob_start();
    require_once ('form.php');
    require_once ('table.php');
    return ob_get_clean();
}
add_shortcode( 'race_registration', 'race_reg' );
