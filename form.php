<form action="" id="race_reg" method="post">
	<!-- ---------------- -->
    <div class="col md-12">
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                   <p class="label">starta numurs</p>

                </div>
                <div class="col md-6">
                  <input type="text" name="reg_data[start_no]">
                </div>
            </div>
        </div>
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label">komandas nosaukums</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[komand_name]">
                </div>
            </div>
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">kategorija</p>
                </div>
                <div class="col md-6">
                    <select name="reg_data[category]" required>
                    	<option ></option>
				      <option value="tourism">TŪRISMS (STANDARTA)</option>
				      <option value="SIDE-BY-SIDE">SIDE-BY-SIDE</option>
                    </select>

        </select>
                </div>
            </div>
        </div>
    </div>
<!-- ------------------- -->

    <div class="col md-12">
        <div class="title">auto</div>

        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">marka</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[marka]" required>
                </div>
            </div>
        </div>
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label">tehniskās pases numurs</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[tech_pass]">
                </div>
            </div>
        </div>

        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">MODELIS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[models]" required>
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">REĢISTRĀCIJAS NR </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[reg_num]" required>
                </div>
            </div>
        </div>



        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label">IZLAIDUMA GADS</p>
                </div>
                <div class="col md-6">
                    <select name="reg_data[auto_gads]" tabindex="12">
        <option></option>
        <option>1970</option>
        <option>1971</option>
        <option>1972</option>
        <option>1973</option>
        <option>1974</option>
        <option>1975</option>
        <option>1976</option>
        <option>1977</option>
        <option>1978</option>
        <option>1979</option>
        <option>1980</option>
        <option>1981</option>
        <option>1982</option>
        <option>1983</option>
        <option>1984</option>
        <option>1985</option>
        <option>1986</option>
        <option>1987</option>
        <option>1988</option>
        <option>1989</option>
        <option>1990</option>
        <option>1991</option>
        <option>1992</option>
        <option>1993</option>
        <option>1994</option>
        <option>1995</option>
        <option>1996</option>
        <option>1997</option>
        <option>1998</option>
        <option>1999</option>
        <option>2000</option>
        <option>2001</option>
        <option>2002</option>
        <option>2003</option>
        <option>2004</option>
        <option>2005</option>
        <option>2006</option>
        <option>2007</option>
        <option>2008</option>
        <option>2009</option>
        <option>2010</option>
        <option>2011</option>
        <option>2012</option>
        <option>2013</option>
        <option>2014</option>
        <option>2015</option>
        <option>2016</option>
        <option>2017</option>
        <option>2018</option>
        <option>2019</option>
        <option>2020</option>
        </select>
                </div>
            </div>
        </div>

     </div><!-- md12 -->

<!-- ------------------------ -->       

    <div class="col md-12">
    	<div class="col md-6">
        	<div class="title">PILOTS</div>
    	</div>
    	<div class="col md-6">
        	<div class="title">STŪRMANIS</div>
    	</div>

        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">VĀRDS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[p_name]" required>
                </div>
            </div>
        </div>
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">VĀRDS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[s_name]" required>
                </div>
            </div>
        </div>

        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">UZVĀRDS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[p_surname]" required>
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">UZVĀRDS </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[s_surname]" required>
                </div>
            </div>
        </div>


         <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">PERSONAS KODS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[p_personal_number]" required>
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">PERSONAS KODS </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[s_personal_number]" required>
                </div>
            </div>
        </div>


<?php if($pro): ?>
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">VALSTS</p>
                </div>
                <div class="col md-6">
                    <select name="pilots_valsts" tabindex="19" required>
        <option></option>
        <option>BELORUS</option>
        <option>DENMARK</option>
        <option>ESTONIA</option>
        <option>FINLAND</option>
        <option>GERMANY</option>
        <option>LATVIA</option>
        <option>LITHUANIA</option>
        <option>NORWAY</option>
        <option>POLAND</option>
        <option>RUSSIA</option>
        <option>SWEDEN</option>
        <option>UNITED KINGDOM</option>
        <option>OTHER / CITS</option>
        </select>
                </div>
            </div>
        </div>


              <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">VALSTS</p>
                </div>
                <div class="col md-6">
                    <select name="sturman_valsts" tabindex="19" required>
        <option></option>
        <option>BELORUS</option>
        <option>DENMARK</option>
        <option>ESTONIA</option>
        <option>FINLAND</option>
        <option>GERMANY</option>
        <option>LATVIA</option>
        <option>LITHUANIA</option>
        <option>NORWAY</option>
        <option>POLAND</option>
        <option>RUSSIA</option>
        <option>SWEDEN</option>
        <option>UNITED KINGDOM</option>
        <option>OTHER / CITS</option>
        </select>
                </div>
            </div>
        </div>
<?php endif; ?>

<div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">TELEFONU NUMURI </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[p_tel_number]" required>
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">TELEFONU NUMURI  </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[s_tel_number]" required>
                </div>
            </div>
        </div>

<div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">E-PASTS </p>
                </div>
                <div class="col md-6">
                    <input type="email" name="reg_data[p_email]" required>
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">E-PASTS </p>
                </div>
                <div class="col md-6">
                    <input type="email" name="reg_data[s_email]" required>
                </div>
            </div>
        </div>
<?php if($pro): ?>
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">VADĪTĀJA APLIECĪBAS NR. </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[p_driver_licence]" required>
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label required">VADĪTĀJA APLIECĪBAS NR.</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[s_driver_licence]" required>
                </div>
            </div>
        </div>

<?php endif; ?>

     </div><!-- md12 -->


<div class="col md-12">
    	<div class="col md-6">
        	<div class="title">STŪRMANIS</div>
    	</div>
    	<div class="col md-6">
        	<div class="title">STŪRMANIS</div>
    	</div>

        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label">VĀRDS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[secondsturman_name]" >
                </div>
            </div>
        </div>
        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label">VĀRDS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[onemore_name]">
                </div>
            </div>
        </div>

        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label">UZVĀRDS</p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[secondsturman_surname]" >
                </div>
            </div>
        </div>


        <div class="col md-6">
            <div class="col md-12 form-cell">
                <div class="col md-6">
                    <p class="label ">UZVĀRDS </p>
                </div>
                <div class="col md-6">
                    <input type="text" name="reg_data[onemoresturman_surname]" >
                </div>
            </div>
        </div>

    <div class="col md-12">

        <input type="checkbox" name="iform"/> <span style="font-size: 1.2em;">Velos saņemt informāciju no MUD Club</span>
        <p></p>
    </div>


</div>
    <div class="col md-12">
        <div class="g-recaptcha" data-sitekey="6LdJ8W4UAAAAAHmfIwaBDpXLpBWmakyKcq7vqwrc"></div>
    </div>

    <input type="submit" value="Submit">
    <div style="height: 30px; display: inline-block;"></div>
</form>


<style>
    #race_reg{
        margin: 50px 0!important;
    }
    .col{
        float: left;
    }
    .col.md-12{
        width: 100%;
    }
    .col.md-6{
        width: 50%;
    }
    #race_reg .title{
        background: #d6aa34;
        padding: 5px 15px;
        color: #342c25;
        margin-top: 20px;
        font-weight: 900;
        font-size: 1.5em;
        margin-bottom: 5px;
    }
    .md-6 .title{
    	text-align: center;
    }
    p.label, .title {
        text-transform: uppercase;
    }
    p.label.required {
        color: #d00100;
    }
    p.label.required:after{
        content: '*';
    }
    .form-cell > div:first-child p{
        padding-left: 15px;
    }
    #race_reg input:not([type='checkbox']), #race_reg select{
        width: 100%;
    }


    @media only screen and (max-width: 959px) {
        .col{
            float: none;
        }
        .col.md-6{
            width: 100%;
        }
    }
</style>


<script>
//    $( document ).ready(function() {
        jQuery("#race_reg").submit(function(event) {
            var recaptcha = jQuery("#g-recaptcha-response").val();
            if (recaptcha === "") {
                event.preventDefault();
                alert("Please check the recaptcha");
            }
        });
//    });


</script>

