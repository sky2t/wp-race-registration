<?php

class registration
{
    private $table_name = 'race_registration';
    private $wpdb ;

    function __construct() {
        global $wpdb;
        $this->wpdb = $wpdb;

            $this->table();

        add_action('wp_ajax_del_request', array( $this, 'delRequest' )
        );
    }

    /**
     * @param string array
     */
    public function getDataById($id)
    {
        return $this->wpdb->get_results( "SELECT * FROM $this->table_name WHERE post_id = $id" );
    }

    /**
     * @return array
     */
    public function getAllData()
    {
        return $this->wpdb->get_results( "SELECT * FROM $this->table_name ORDER by post_id" );
    }

    public function getUniqIds()
    {
        return $this->wpdb->get_results( "SELECT DISTINCT post_id FROM $this->table_name ORDER by post_id DESC" );
    }

    public function addData($id, $data)
    {

        $col= array(
            'post_id' => $id,
            'data' => json_encode($data),
            'ip' => $_SERVER['REMOTE_ADDR']
        );

        $this->wpdb->insert($this->table_name,$col);

        $this->mail($data);
    }

    public function delRequest(){
        if($_REQUEST['id']){
            echo $this->wpdb->delete( $this->table_name, array( 'id' => $_REQUEST['id'] ) );
        }

        exit();
    }

    public function today()
    {
        return $this->wpdb->get_var("SELECT COUNT(id) FROM $this->table_name WHERE DATE(reg_date) = DATE(NOW())");
    }

    public function allTime()
    {
        return $this->wpdb->get_var("SELECT COUNT(id) FROM $this->table_name");
    }

    private function table(){
        if($this->wpdb->get_var("SHOW TABLES LIKE '$this->table_name'") != $this->table_name) {

            //table not in database. Create new table
            $charset_collate = $this->wpdb->get_charset_collate();

            $sql = "CREATE TABLE $this->table_name (
          id bigint(20) NOT NULL AUTO_INCREMENT,
          post_id bigint(20) NOT NULL,
          data longtext NOT NULL,
          ip varchar(20) NOT NULL,
          reg_date timestamp NULL DEFAULT CURRENT_TIMESTAMP,
          UNIQUE KEY id (id)
     ) $charset_collate;";
            require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
            dbDelta( $sql );
        }
    } //table

    public function settings($arr = array()){

        $settings = fopen(__DIR__.DIRECTORY_SEPARATOR."settings.txt", 'w');
        fwrite($settings,json_encode($arr , JSON_PRETTY_PRINT));
        fclose($settings);
    }


    public function sturmans($value = null)
    {
        if ($value && !empty($value)) {
            $result = '';
            foreach ($value as $key => $name) {
                if ($name != '') {
                    if ($key > 0) {
                        $result .= ', ' . $name;
                    }else{
                        $result .=  $name;
                    }
                }
            }
            return $result;
        }
    } // sturmans

    public function subscribe($data = null)
    {
        if($data){

            $authToken = 'e75051df11965a15803551fd0bec973a-us17';
            $list_id = '7b2620afd4';
// The data to send to the API
            $postData = array(
                "email_address" => $data['p_email'],
                "status" => "subscribed",
                "merge_fields" => array(
                    "NAME"=> $data['p_name'] . " " . $data['p_surname'],
                    "PHONE"=> $data['p_tel_number'])
            );

// Setup cURL
            $ch = curl_init('https://us17.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
            curl_setopt_array($ch, array(
                CURLOPT_POST => TRUE,
                CURLOPT_RETURNTRANSFER => TRUE,
                CURLOPT_HTTPHEADER => array(
                    'Authorization: apikey '.$authToken,
                    'Content-Type: application/json'
                ),
                CURLOPT_POSTFIELDS => json_encode($postData)
            ));
// Send the request
            $response = curl_exec($ch);
            return $response;
        }

    }

    protected function mail($data){

        $to      = get_option( 'admin_email');
        $subject = 'NEW participant';
        $message = $data['p_name']. " ". $data['p_surname']. " on " . $data['marka'] ." ". $data['models']. "\r\n";
        $headers  = "Content-type: text/html; charset=UTF-8 \r\n";
        $headers .= 'From: registration form <'.$data["p_email"].'>' . "\r\n";

        mail($to, $subject, $message, $headers);

        $headers2  = "Content-type: text/plain; charset=UTF-8 \r\n";
        $headers2 .= 'From: mudclub.lv <'.$to.'>' . "\r\n";
        mail($data["p_email"], 'Paldies! Jūsu pieteikums veiksmīgi nosūtīts!', 'Paldies! Jūsu pieteikums veiksmīgi nosūtīts!', $headers2);
    }

} // class