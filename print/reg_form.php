<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<link rel="Stylesheet" href="<?= plugin_dir_url(__FILE__) ?>stylesheet.css">



</head>

<body>
<!--<div class="thumb" style="position: absolute; top: 5px;"></div>-->

<table border=0 cellpadding=0 cellspacing=0 style='border-collapse:
 collapse;table-layout:fixed;display: table-caption;width:690pt'>
<tr style="margin-bottom: -65px; display: inline-block;"><td class="thumb" colspan=1 ></td></tr>

 <tr height=20 style='height:15.0pt'>
  <td colspan=12 height=40 class="xl73 event"  style='height:40pt;'>Название
  мероприятия</td>
 </tr>
 <tr height=21 style='height:15.75pt'>
  <td colspan=12 height=21 class="xl73 excert" style='height:15.75pt'>Дата</td>
 </tr>
 <tr height=21 style='height:15.75pt'>
  <td colspan=12 height=21 class=xl70 style='border-right:1.0pt solid black;
  height:15.75pt'>Pieteikums / Application / Заявка</td>
 </tr>
 <tr style='height:30.75pt'>
  <td colspan=1 class=xl66 width=188 style='height:30.75pt;border-top:none;
  width:181pt'>Borta numurs / Start number / Бортовой номер</td>
  <td colspan=1 class="xl67 start_no" style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl66 style='border-top:none;border-left:none;'>Kategorija
  / Class / Зачётная группа</td>
  <td colspan=2 class="xl79  category " style='border-right:1.0pt solid black;border-left:
  none'>&nbsp;</td>
 </tr>


 <tr height=76 style='mso-height-source:userset;height:57.0pt'>
  <td colspan=1 height=76 class=xl66 width=188 style='height:57.0pt;border-top:none;
  width:141pt'>Pieteikums sa&#326;emts / Application received / Заявка получена</td>
  <td colspan=1 class="xl67 reg_date" style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class="xl66" width=131 style='border-top:none;border-left:none;width:98pt'>Komandas
  nosaukums / Team name / Название команды</td>
  <td colspan=2 class="xl79  komand_name" style='border-right:1.0pt solid black;border-left:
  none'>&nbsp;</td>
 </tr>

 <tr height=21 style='height:15.75pt'>
  <td colspan=12 height=21 class=xl74 style='border-right:1.0pt solid black;
  height:15.75pt'>Auto / Vehicle / Автомобиль</td>
 </tr>

 <tr height=87 style='mso-height-source:userset;height:65.25pt'>
  <td colspan=1 height=87 class="xl68" style='height:65.25pt;border-top:none'>Modelis /
  Model / Модель</td>
  <td colspan=1 class="xl67 marka models" style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class="xl66" width=131 style='border-top:none;border-left:none;width:98pt'>Tehnisk&#257;s
  pases Nr / Technical passport Nr / Номер техпаспорта</td>
  <td colspan=2 class="xl79 tech_pass" style='border-right:1.0pt solid black;border-left:
  none'>&nbsp;</td>

 </tr>
 <tr height=61 style='height:45.75pt' class="pro">
  <td colspan=1 height=61 class=xl66 width=188 style='height:45.75pt;border-top:none;
  width:141pt'>Izlaiduma gads / year of manufacture / Год выпуска</td>
  <td colspan=1 class="xl67 auto_gads" style='border-top:none;border-left:none'>&nbsp;</td>
  <td colspan=2 class=xl66 width=131 style='border-top:none;border-left:none;width:98pt'>Re&#291;istr&#257;cijas
  Nr / Number plate / Номер машины</td>
  <td colspan=2 class="xl79 reg_num" style='border-right:1.0pt solid black;border-left:
  none'>&nbsp;</td>
 </tr>

 <tr height=21 style='height:15.75pt'>
  <td colspan=12 height=21 class=xl77 style='height:15.75pt'>Inform&#257;cija
  par ekip&#257;&#382;u / Crew list / Информация о членах экипажа</td>
 </tr>

 <tr height=41 style='mso-height-source:userset;height:30.75pt'>
  <td colspan="1" height=41 style='height:30.75pt'></td>
  <td colspan="1" class=xl66 width=150 style='width:150pt'>Pilots / Pilot / Пилот</td>
  <td colspan="1" class=xl66 width=131 style='border-left:none;width:98pt'>1.St&#363;rmanis
  / 1st Co-Pilot / 1й штурман</td>
  <td class=xl66 width=138 style='border-left:none;width:104pt'>2.St&#363;rmanis
  / 2nd Co-Pilot / 2ой штурман</td>
  <td class=xl66 width=131 style='border-left:none;width:98pt'>3.St&#363;rmanis
  / 3rd Co-Pilot / 3й штурман</td>
  
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl69 width=188 style='height:30.0pt;width:141pt'>V&#257;rds,
  Uzv&#257;rds / Name, Surname / Имя, Фамилия</td>
  <td class="xl67 p_name p_surname" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 s_name s_surname" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 secondsturman_name secondsturman_surname" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 onemore_name onemoresturman_surname" style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl69 width=188 style='height:30.0pt;border-top:none;
  width:141pt'>Personas kods / ID number / Персональный код</td>
  <td class="xl67 p_personal_number" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 s_personal_number" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 " style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 " style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl69 width=188 style='height:30.0pt;border-top:none;
  width:141pt'>Telefonu numuri / Phone numbers / контактные телефоны</td>
  <td class="xl67 p_tel_number" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 s_tel_number" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 " style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 " style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl69 width=188 style='height:30.0pt;border-top:none;
  width:141pt'>E-pasts / E-mail / Электронная почта</td>
  <td class="xl67 p_email" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 s_email" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 " style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 " style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=81 style='height:60.75pt' class="pro">
  <td height=81 class=xl69 width=188 style='height:60.75pt;border-top:none;
  width:141pt'>Vad&#299;t&#257;ja Apliec&#299;bas Nr. / Driving licence Nr. / №
  водительского удостоверения</td>
  <td class="xl67 p_driver_licence" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67 s_driver_licence" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67" style='border-top:none;border-left:none'>&nbsp;</td>
  <td class="xl67" style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=61 style='height:45.75pt' class="pro">
  <td height=61 class=xl69 width=188 style='height:45.75pt;border-top:none;
  width:141pt'>Sportista licences Nr.<span style='mso-spacerun:yes'>  </span>/
  Competitor licence Nr / № спортивной лицензии</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=61 style='height:45.75pt' class="pro">
  <td height=61 class="xl69" width=188 style='height:45.75pt;border-top:none;
  width:141pt'>Apdro&#353;in&#257;&#353;anas licences Nr / Insurance policy Nr
  / № страхового полиса</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>

 <tr height=145 style='mso-height-source:userset;height:0pt'>
  <td colspan=12 height=145 class="xl78 pro"  >Parakstot &#353;o pieteikumu visas personas, kuras atrodas
  transporta l&#299;dzekl&#299; ap&#326;em&#257;s pas&#257;kum&#257; laik&#257;
  iev&#275;rot Latvij&#257; sp&#275;k&#257; eso&#353;os CSN un uz&#326;emas
  pilnu atbild&#299;bu par savu darb&#299;bu vai bezdarb&#299;bu, k&#257;
  ar&#299; atsak&#257;s no visam pretenzij&#257;m pret organizatoru un FIA
  NEZ<span style='mso-spacerun:yes'>  </span>ofici&#257;laj&#257;m
  person&#257;m, k&#257; ar&#299; apliecina, ka pieteikum&#257;
  min&#275;t&#257; inform&#257;cija<span style='mso-spacerun:yes'>  </span>ir
  pareiza. Paraksto &#353;o pieteikumu ekip&#257;&#382;a piekr&#299;t, ka Ka
  Latvijas Automob&#299;&#316;u Feder&#257;cijas Trofi Reida komisija
  apstr&#257;d&#257; ekip&#257;&#382;as personas datussekojo&#353;iem
  m&#275;r&#311;iem: &#353;o sacens&#299;bu, Latvijas trofi reida
  &#269;empion&#257;ta un NEZ trofi reida &#269;empion&#257;ta
  organiz&#275;&#353;anai. Parakstot &#353;o pieteikumu piekr&#299;tu
  sacens&#299;bu foto un video izmanto&#353;anai auto sporta
  populariz&#275;sanai. Dal&#299;bnieku dati netiks nodoti tre&#353;aj&#257;m
  person&#257;m.<span style='mso-spacerun:yes'> </span></td>
  
 </tr>
 <tr height=160 style='mso-height-source:userset;height:0pt'>
  <td colspan=12 height=160 class="xl78 pro"  >All persons who are in the vehicle during the competition assure
  that they will follow road traffic rules in force in the territory of Latvia
  and take all the risks for their actions or inactions and renounce in advance
  all responsibility on the part of the organizers , its oficials the FIA NEZ.
  All persons who are in the vehicle during the competition assure that
  information included in this form is true. By signing this application team
  agree to process their personal data by Latvian Automobile Federation Trophy
  Raid commission<span style='mso-spacerun:yes'>  </span>for the following
  purposes: Organization of this competition, Latvia Trophy Raid championship.
  By signing this application team authorizes the use of photo and video<span
  style='mso-spacerun:yes'>  </span>materials of the event for the purpose of
  popularization of motoring. Competitor's personal data shall not be given to
  the third parties.</td>
  
 </tr>
 <tr height=196 style='mso-height-source:userset;height:0pt'>
  <td colspan=12 height=196 class="xl78 pro"  >Подписывая данное заявление, все члены экипажа, которые
  находятся в автомобиле во время соревнований, обязуются соблюдать ПДД,
  существующие в Латвийской республике и нести полную ответственность за свои
  действия или бездействия. Так же все члены экипажа отказываются от
  предъявления какого - либо рода претензий организаторам , их представителям
  или должностным лицам FIA NEZ, и подтверждают что вся информация в данном
  заявленинии актуальна и соответствует действительности. Подписывая данное
  заявление, все члены экипажа разрешают коммиссии по трофи рейдам Латвийской
  автомобильной федерации обрабатывать их личные данные с целью организации
  этих соревнований, Чемпионата Латвии<span style='mso-spacerun:yes'> 
  </span>по трофи рейдам , а так же NEZ чемпионата. Подписывая данное
  заявление, все члены экипажа согласны что все фото и видео материалы могут
  быть использованы в целях популяризации автоспорта. Организатор соревнований
  подтверждает что персональные данные участников не будут переданы третьим
  лицам.<span style='mso-spacerun:yes'> </span></td>
  
 </tr>

 <tr height=196 style='mso-height-source:userset;height:0pt'>
     <td colspan=12 height=196 class="xl78 regular"  >Parakstot šo pieteikumu visas personas, kuras atrodas transporta līdzeklī apņemās pasākumā laikā ievērot
         Latvijā spēkā esošos CSN un uzņemas pilnu atbildību par savu darbību vai bezdarbību, kā arī atsakās no visam
         pretenzijām pret organizatoru un / vai trešajām personam. Parakstot šo pieteikumu visas personas, kuras atrodas
         transporta līdzeklī piekrīt , ka Organizators nenes nekādu atbildību ne par kādiem zaudējumiem, ko ekipāža
         radījusi sev, trešajām personām, vai ko trešās personas radījušas ekipāžai, sekām, kādas tās būtu, tai skaitā
         par veselības un/vai dzīvības un materiālajiem kaitējumiem sev un jebkurai trešajai personai . Apliecinam, ka
         pieteikumā minētā informācija ir pareiza. Parakstot šo pieteikumu ekipāža piekrīt, ka Biedrība "MUD Club"
         apstrādā ekipāžas personas datus sekojošiem mērķiem: šo sacensību vai braucienu un / vai Latvijas trofi reida
         čempionāta organizēšanai. Parakstot šo pieteikumu piekrītu pasākuma foto un video izmantošanai auto sporta
         popularizēsanai. Dalībnieku dati netiks nodoti trešajām personām. <span style='mso-spacerun:yes'> </span></td>
     
 </tr>

 <tr height=40 style='mso-height-source:userset;height:30.0pt'>
  <td height=40 class=xl69 width=188 style='height:30.0pt;width:141pt'>V&#257;rds,
  Uzv&#257;rds / Name, Surname / Имя, Фамилия</td>
  <td class="xl67 p_name p_surname" style='border-left:none'>&nbsp;</td>
  <td class="xl67 s_name s_surname" style='border-left:none'>&nbsp;</td>
  <td class="xl67 secondsturman_name secondsturman_surname " style='border-left:none'>&nbsp;</td>
  <td class="xl67 onemore_name onemoresturman_surname " style='border-left:none'>&nbsp;</td>
  
 </tr>
 <tr height=41 style='height:30.75pt'>
  <td height=41 class=xl69 width=188 style='height:30.75pt;border-top:none;
  width:141pt'>Datums, Paraksts / Date , Signature / Дата, подпись</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  <td class=xl67 style='border-top:none;border-left:none'>&nbsp;</td>
  
 </tr>

</table>

</body>

</html>
