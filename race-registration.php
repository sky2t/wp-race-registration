<?php
/*
Plugin Name: Wp Race Registration
Description: race registration
Author: SkyNet
Text Domain: wp-race-registration
Domain Path: /languages/
Version: 1.6
*/

require_once ('shortcode.php');
require_once ('registration.php');

add_action( 'admin_menu', function () {
    add_menu_page( 'Race Registration', 'Race Registration',  'manage_options', 'race_reg', 'rr_main', 'dashicons-editor-table', '23' );  //пункт в главном меню
    add_submenu_page('race_reg', 'All record' , 'All record', 'manage_options', 'statistic', 'rr_statistic');
});
$reg = new registration();

function rr_main(){
    global  $reg;
    ?>
    <div class="wrap">
            <h2>Race Registration</h2>

        <h3>Shortcode: </h3>
        <h4>Regular Race: <code>[race_registration]</code></h4>
        <h4>PRO Race: <code>[race_registration form="pro"]</code></h4>

        <div class="card-group">
            <div class="card">
                <div class="counter counter-lg text-left pl-20">
                    <span class="counter-number"><b><?= $reg->today() ?></b></span>
                    <div class="counter-label text-uppercase">Today</div>
                </div>
            </div>
            <div class="card">
                <div class="counter counter-lg text-left pl-20">
                    <span class="counter-number"><b><?= $reg->allTime() ?></b></span>
                    <div class="counter-label text-uppercase">All time</div>
                </div>
            </div>
        </div>
    </div>

    <style>
        .wrap{
            padding: 30px!important;
        }
        .card-group .card{
            float: left;
        }
    </style>

    <?php
}

function rr_statistic(){
    global  $reg;
    wp_enqueue_style('admin-css', plugin_dir_url(__FILE__) . 'race-admin.css');
    wp_enqueue_script('admin-js', plugin_dir_url(__FILE__) . 'race-admin.js');
    ?>
    <div class="wrap">
        <h2>Race Registration</h2>
    <?php

    foreach ($reg->getUniqIds() as $IDs){
        $post_data = [
            "name" => get_post( $IDs->post_id)->post_title,
            "excert" => get_post( $IDs->post_id)->post_excerpt,
            "thumbnail" => get_the_post_thumbnail($IDs->post_id)
        ];
        echo "<h3>". $post_data['name'] ."</h3>";
        echo "<h4>". $post_data['excert'] ."</h4>";
?>
        <table class="widefat fixed striped race-table">
            <thead>
        <tr>
            <th width="1"></th>
            <th>NR</th>
            <th>PILOTS</th>
            <th>TELEFONS</th>
            <th>STŪRMAŅI</th>
            <th>KOMANDAS NOSAUKUMS</th>
            <th>AUTO</th>
            <th>REG DATE</th>
            <th class="ip">IP</th>
            <th width="10"></th>
        </tr>
            </thead>
            <tbody>
        <?php
        foreach ($reg->getDataById($IDs->post_id) as $arr) {
            $data = json_decode($arr->data);
            ?>

                <tr id="<?= $arr->id ?>" class="<?= $data->p_driver_licence ? 'pro' : 'regular' ?>" data-info='<?= $arr->data ?>' data-post='<?=  json_encode($post_data) ?>' data-reg='<?= $arr->reg_date ?>'>
                    <td><?= $data->start_no ?></td>
                    <td><?= $data->p_name. ' '. $data->p_surname ?></td>
                    <td><?= $data->p_tel_number ?></td>
                    <td><?= $reg->sturmans([$data->s_name, $data->secondsturman_name, $data->onemore_name]) ?></td>
                    <td><?= $data->komand_name ?></td>
                    <td><?= $data->marka .' '. $data->models ?></td>
                    <td><?= $arr->reg_date ?></td>
                    <td class="ip"><?= $arr->ip ?></td>
                    <td class="del"><span class="dashicons dashicons-no del" style="color: red;"></span></td>
                </tr>

            <?php
        }
        ?>
            </tbody>
        </table>
        <hr>

            <?php
    }
    ?>
    </div>
    <div id="form" class="print">
        <div class="form-body">
            <div class="menu">
                <span class="btn close_btn" onclick="document.getElementById('form').style.display = 'none'">CLOSE</span>
    <!--            <span onclick="window.print()" class="btn print_btn">PRINT</span>-->
                <span onclick="reg_print()" class="btn print_btn">PRINT</span>

            </div>
            <div class="content">
                <?php  require_once('print/reg_form.php') ?>
            </div>
        </div>
    </div>

<?php
}



wp_enqueue_script( 'script', 'https://www.google.com/recaptcha/api.js');
