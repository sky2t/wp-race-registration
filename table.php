<?php

 $arr = $reg->getDataById($id);
 if(!empty($arr)):


?>
<table>
    <tr>
        <th>NR</th>
        <th>PILOTS</th>
        <th>STŪRMAŅI</th>
        <th>KOMANDAS NOSAUKUMS</th> 
        <th>AUTO</th>
    </tr>
    <?php
    foreach ($arr as $a) {
        $data = json_decode($a->data);
?>
    <tr>
        <td><?= $data->start_no ?></td>
        <td><?= $data->p_name ?></td>
        <td><?= $reg->sturmans([$data->s_name, $data->secondsturman_name, $data->onemore_name]) ?></td>
        <td><?= $data->komand_name ?></td>
        <td><?= $data->marka .' '. $data->models ?></td>
    </tr>

    <?php  } ?>
    
</table>

<div style="height: 50px; display: inline-block;"></div>


<?php
endif;